#include <commonsense.h>

#define WAIT 10000000

int main(void) {

    cs_led_setup();

    cs_led_off(LED_RED);
    cs_led_off(LED_GREEN);
    cs_led_off(LED_BLUE);

    while(1) {
        cs_led_on(LED_RED);
        for (int i = WAIT; i; --i);
        cs_led_off(LED_RED);
        cs_led_on(LED_GREEN);
        for (int i = WAIT; i; --i);
        cs_led_off(LED_GREEN);
        cs_led_on(LED_BLUE);
        for (int i = WAIT; i; --i);
        cs_led_off(LED_BLUE);
    }
    
    return 0;
}
