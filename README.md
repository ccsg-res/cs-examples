# CommonSense Examples

This repository contains PlatformIO examples for aiding firmware development on the CommonSense platform, designed by the CCSG Research Group at Carnegie Mellon University. 

Each directory contains a PIO project. To run the project, open the project in PlatformIO, build, and upload the file onto a CommonSense board. Please refer to the CommonSense wiki for more detailed instructions.